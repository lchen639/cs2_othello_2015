Describe how and what each group member contributed for the past two weeks. 
Document the improvements that your group made to your AI to make it tournament-worthy.
Explain why you think your strategy(s) will work. 
Feel free to discuss any ideas were tried but didn't work out.

I worked solo. In the past two weeks, I came up with some ideas for the development of my Othello AI and
wrote a simple program to complete tasks listed in part 1 of the Othello assignment.
I got into a time crunch with other commitments/finals, so I was not able to fully flush my ideas out.
My first two ideas were not fully thought out, so I will focus on my final idea. Note that all of my scratchwork is
inserted below. In essence, my final idea contained minimax along with a transposition table to save some time (i.e. it prevented
duplicate boards to prevent excessive branching, saving time and memory). It would choose corners whenever they 
were available legal moves, but otherwise use scoring results as part of minimax. I couldn't figure out a good 
hash function for the transposition table. For minimax, I wanted to do a depth-first search with a stack, but time
constraints prevented me from finishing it. I was also going to attempt alpha-beta pruning and experiment with
checking opponent mobility (which could have been done simply by counting the number of opponent move boards
in the tree) along with my own mobility (same idea), but I could not figure out where to store all this information
in a neat and easily accessible way.

Below, please find my scratchwork (psuedocode and thoughts).

8x8 array table for "weighting" -- (+1) for stable (-1) for unstable
reference[8][8]
> Could alternatively just make a 64 element vector (seems better actually),
> iteratiting as reference[(y * 8) + x]

std::vector<int> reference

make illegal moves worth -1000

> Make a vector of vectors (v^2) to check if already studied (transposition)
    
    std::vector<vector??????>
    To check in v^2: 
    vector.find() might not work? 
        std::vector< std::vector<int> > test = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        if(test.find({1, 2, 3}) == test.end())
        {
            printf("fuck");
        }
        else
        {
            printf("%i", test.find({1, 2, 3}));
        }

    Instead use iterators?
    
    
Attempt 2=========================================================================
PRIORITY: SECURE CORNERS

RESTRICT OPPONENT MOVES

for (int i = 0; i < 8; i++)     // set as x direction
        {
            for (int j = 0; j < 8; j++) // set as y direction
            {
                maiMove = new Move(i,j);

                if(bd.checkMove(maiMove, maiSide))
                {
                    // find the corresponding piece for capture
                    // will do at later point.

                    CALL SOME FUNCTION
                    bd.doMove(maiMove, maiSide);
                    return maiMove;
                }
            }
        }




Restart 3=======================================================================

Priorities:
Obtain corners

Create set of boards for record (prevent overlap)
Alpha Beta Pruning based on score value, opponent, and self mobility recorded (in heuristic)

At some point, need function to return: Array (score, opp_mot, self_mot) 

First, we will try to figure out what to do with minimax

int * minimax(Board given, ) // dunno args passed in, but should return best next move
{
    int * boardArr[64] = converted(given); // this creates "boards" we will work with to figure out what to do
    int minimalLoss;
    int tempSum;
    int * bestOption; // pointer to best "first" move thus far

    awesomesauce(boardArr, opponent_Color, SET, TREE, maiColor, 1);

    TRAVERSE THROUGH TREE AND ADD END SCORES FROM INDIVIDUAL FIRST STEPS
    if(tempSum > minimalLoss)
    {
        bestOption = currentFirstStep;
    }

    return bestCurrentOption;
}

Function creating all possible branches from given board.
    (start counter at 1)
TREE awesomesauce(int * board, mostRecentSide, SET, TREE, maiColor, counter)
{
    currentSide = OPPOSITE of mostRecentSide

    for (int i = 0; i < 8; i++)     // set as x direction
    {
        for (int j = 0; j < 8; j++) // set as y direction
        {
            if(MOVE LEGAL (separate function return T/F) && NOT IN SET (separate function return T/F using transposition table))
            {
                CHANGE boardarr accordingly
                get SCORE, opponent motility (if applicable) 
                ADD to SET (separate function; transposition table)
                ADD to TREE (same function, shift some pointer in tree around linked list, include SCORE)

                if(counter != 4) // final is opponent's step
                {
                    CALL awesomesauce(boardarr, currentSide, SET, TREE, maiColor counter++);
                }
            }
        }
    }
    // HAVING REACHED END
    move TREE pointer back, break out of layer of recursion.
}

Board translateToBoardType(int * boardArray)
{
    int arrayPosition = 0;
    Board translated;
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            if(boardArray[arrayPosition] != 0)  //NEED TO DEAL WITH THIS
            {
                TRANSLATE boardArray number to WHITE OR BLACK, put on tempbd(i,j);
                return 
            }
            arrayPosition++;
        }
    }
    return translated;
}

bool moveLegal(x, y, currentSide, int * boardArray)
{
    Move *test = new Move(x,y);
    Board tempbd = translateToBoard(boardArray);

    return tempbd.checkMove(test, currentSide);
}

bool inSet(int * boardArray) //returns true if boardArray already in set
{
    // TODO: HASH FUNCTION?!

    std::unordered_set<int *>::const_iterator contain = SET.find(boardArray);
    if(contain != SET.end())
    {
        return true;
    }
    return false;
}

void addToSet(int * boardArray, SET)
{
    Board toSet = translateToBoard(boardArray);

    // TODO: HASH FUNCTION?!

    ADD to proper container of SET
}

int hashfunction(myColor, int * boardToHash)
{
    // hashes based on my score
    if (myColor == BLACK)
    {
        return boardToHash.countBlack();
    }

    else
    {
        return boardToHash.countWhite();
    }
}

Function giving score of end boards (after seeing x ahead)
int scoring(myColor, TREE_NODE/int * boardArray, int * original)    // what to do with my color?
{
    Board originalBoard = translateToBoard(original);
    Board toScore = translateToBoard(boardArray);

    if (myColor == BLACK)
    {
        return (originalBoard.countBlack() - toScore.countBlack());
    }

    else
    {
        return (originalBoard.countWhite() - toScore.countWhite());
    }
}

Function returning optimized move to complete
uhhhh choose side with lowest cost?