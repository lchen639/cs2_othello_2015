#include "player.h"
#include <stdio.h>
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    
    if (side == BLACK)
    {
        maiSide = side;
        oppSide = WHITE;
    }

    else
    {
        maiSide = side;
        oppSide = BLACK;
    }
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    
    bd.doMove(opponentsMove, oppSide); // Updates board first
    // int topFlips; // I'll change this when I have more time
    Move *maiMove = NULL;
    // int x = 0, y = 0, border; // I'll change this when I have more time

    if(bd.hasMoves(maiSide))
    {
        for (int i = 0; i < 8; i++)     // set as x direction
        {
            for (int j = 0; j < 8; j++) // set as y direction
            {
                maiMove = new Move(i,j);

                if(bd.checkMove(maiMove, maiSide))
                {
                    // find the corresponding piece for capture
                    // will do at later point.
                    bd.doMove(maiMove, maiSide);
                    return maiMove;
                }
            }
        }    
    }

    return NULL;
}
